import React, { Component, useEffect, useState } from 'react'
import { Text, View , TextInput, Button} from 'react-native'



const Screen2 = ({navigation,route}) => {
    const [data, setData] = useState('dsadasd')

    useEffect(() => {
        console.log(route)
    
    }, [])
    return (
        <View>
        <Text> This is Screen2 </Text>
        <TextInput
            placeholder ="Text here"
            style = {{
                backgroundColor : 'blue'
            }}
            onChangeText = {(e) => (setData(e))}
            value = {data}
        />
        <Button title="Back" 
        onPress= {() => navigation.navigate('screen1', {data : data})}
        />
            
    </View>
    )
}

export default Screen2


