import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import ScreenTab1 from './ScreenTab1'
import Screen2 from './Screen2';



const Stack = createStackNavigator()

const index = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name = "screen1" component={ScreenTab1}/>
            <Stack.Screen name = "screen2" component={Screen2} />
          </Stack.Navigator>
    )
}

export default index

const styles = StyleSheet.create({})
