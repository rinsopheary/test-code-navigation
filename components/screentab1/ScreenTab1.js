import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Button, Alert } from 'react-native'

const ScreenTab1 = ({navigation, route}) => {

    const {data} = route?.params
    const {container} = styles
useEffect(() => {
    console.log(route)

}, [])
    return (
        <View style={container}>
            <Text>{data}</Text>
            <Button
                onPress ={() => navigation.navigate('screen2')}
                title ="Go"
            ></Button>
        </View>
    )
}

export default ScreenTab1

const styles = StyleSheet.create({
    container : {
        flex : 1,
        alignItems : "center",
        justifyContent : "center"
    }
})
