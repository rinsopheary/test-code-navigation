import { NavigationContainer } from '@react-navigation/native'
import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Tab1 from './components/screentab1';
import Tab2 from './components/Tab2';


const Tab = createBottomTabNavigator();
export default class App extends Component {
  render() {
    
    return (
      // <SafeAreaView>
        <NavigationContainer>
          <Tab.Navigator>
            <Tab.Screen name = "Tab1" component={Tab1}/>
            <Tab.Screen name = "Tab2" component={Tab2}/>
          </Tab.Navigator>
        </NavigationContainer>
      // </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({})
